# esay

#### 介绍
基于webmagic的通用爬虫抓取应用，核心在于简单易用，搭建好后轻松抓取数据

#### 在线演示地址
在线演示地址: http://easycrawl.lomoye.top/
(在线演示地址第一次打开有点慢，请耐心等待)

#### 教程文档
教程文档地址: http://blog.lomoye.top/index.php/archives/15/

#### 前端代码
https://gitee.com/mountFuji/easy-crawl-front

#### 软件架构
软件架构说明
- 基于springboot实现
- 爬虫框架使用的是webmagic
- 数据库默认的是h2，配置文件在application.yml，如果想切换成mysql，请参考分支useMysql里的application.yml配置

#### 安装教程
1. EasyApplication 启动入口
2. 浏览器打开localhost:8080
3. 详细使用方式请看文档 http://blog.lomoye.top/index.php/archives/15/

#### 使用说明
- 基本流程
    1. EasyApplication 启动入口
    2. 打开 localhost:8080
    3. 新建爬虫(默认会导入两个爬虫示例)
    4. 运行
    5. 查看任务
    
#### 联系方式
- 微信： yezhangjun001
- QQ:   834033206
    
#### FAQ
- 如何查看数据库数据：默认是使用嵌入式的h2数据库，可以用浏览器打开http://localhost:8080/h2-console登录, 默认用户名root，默认密码test

- 如何替换数据源: 默认是使用嵌入式的h2数据库，如果想替换成mysql，请在application.yml中更改数据源配置

#### 页面预览
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/32b4fc5709144a49ba7c40ce4111dbb8.jpg)
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/5b7a40d7f8854d8ea9cde55283d8ef26.jpg)
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/0501423989664b2a84c4db7383478216.jpg)
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/02d4c1a57e1a4cbb88b605ddddbdc422.jpg)
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/df8c39379fab487ea7f75cbcb6542c6d.jpg)

#### 参与贡献
1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 最近待修复问题

#### 最近完成功能
1.创建修改爬虫时增加爬虫字段的测试爬取功能

#### 待优化
1.字段长度有些可能偏长，现在默认最多存储255个字节，应该提供用户选择字段长度
2.可以定义字段的类型，比如图片链接，这样在显示的时候可以直接根据图片属性显示出图片

#### 待修复的bug

#### 已修复的bug
1.解决豆瓣爬取报403的问题，403不是因为豆瓣防爬，而是因为0.7.3版本的webmagic的SSL协议只支持TLSv1.0，自己重写了逻辑，可以支持TLSv1.2




#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)